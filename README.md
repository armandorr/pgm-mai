# PGM - You Only Derive Once (YODO)

Probabilistic Graphical Models (PGM) - Master in Artificial Intelligence

Presentation and video explanation of the the paper:

- You Only Derive Once (YODO): Automatic Differentiation for Efficient Sensitivity Analysis in Bayesian Networks from Rafael Ballester-Ripoll and Manuele Leonelli: https://doi.org/10.1016/j.ijar.2023.108929

## Authors
- Armando Rodriguez
- Hasnain Shafqat